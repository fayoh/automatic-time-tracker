package com.example.autotime.test;

import com.example.autotime.Overview;

import android.database.Cursor;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class TodayTest extends ActivityInstrumentationTestCase2<Overview> {

	private Overview mTodayActivity;
	private Spinner  mTaskSelector;
	private ListView mTimeStampList;
	private Button   mCheckInOutButton;
	
	public TodayTest() {
		super("com.example.autotime", Overview.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setActivityInitialTouchMode(false);
		mTodayActivity    = getActivity();
		mTaskSelector     = (Spinner)
				mTodayActivity.findViewById(com.example.autotime.R.id.taskSelector);
		mTimeStampList    = (ListView)
				mTodayActivity.findViewById(com.example.autotime.R.id.timestampstoday);
		mCheckInOutButton = (Button)
				mTodayActivity.findViewById(com.example.autotime.R.id.checkinout);
	}
	
	/**
	 * Testcase for activitys state after creation.
	 */
	public void testCreate() {
		assertNotNull(mTodayActivity);

		assertNotNull(mTaskSelector);
		assertEquals(3, mTaskSelector.getCount());
		assertEquals(1, mTaskSelector.getSelectedItemId());
		assertEquals("Project 1", ((TextView)mTaskSelector.getSelectedView()).getText());

		assertNotNull(mTimeStampList);
		
		assertNotNull(mCheckInOutButton);
		assertEquals(true, mCheckInOutButton.isEnabled());
	}
	
}
