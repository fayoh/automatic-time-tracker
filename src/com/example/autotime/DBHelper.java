package com.example.autotime;

import java.util.Calendar;

import com.example.autotime.DBSchema.Task;
import com.example.autotime.DBSchema.Timestamp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "timestamp.db";
    
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DBSchema.SQL_CREATE_TASK_TABLE);
		db.execSQL(DBSchema.SQL_CREATE_TIMESTAMP_TABLE);
		db.execSQL(DBSchema.SQL_CREATE_WORKPLACE_TABLE);
		db.execSQL(DBSchema.SQL_POPULATE_TASK_TABLE1);
		db.execSQL(DBSchema.SQL_POPULATE_TASK_TABLE2);
		db.execSQL(DBSchema.SQL_POPULATE_TASK_TABLE3);
		db.execSQL(DBSchema.SQL_CREATE_TIMESTAMP_VIEW);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Implement when v2 of db comes along
		// Create any new tables or views 
		onCreate(db);
	}
	
	public Cursor getTimestampsToday(SQLiteDatabase db) {
		//Get start of day (midnight)
		//TODO add feature to select start of day?
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND,0);
		return db.query(DBSchema.TIMESTAMP_TASK_VIEW_NAME,
						null,
						Timestamp.COLUMN_NAME_INTIME + " > ?",
						new String[] {String.valueOf(cal.getTimeInMillis())},
						null, null, null);
	}
	
	public void addTimestamp(SQLiteDatabase db, long timeInMilliS, int taskId) {  	
    	// Check current state
		Cursor cursor =
				db.query(Timestamp.TABLE_NAME,
						 new String[] {Timestamp._ID, Timestamp.COLUMN_NAME_OUTTIME},
						 null,
						 null,
						 null,
						 null,
						 Timestamp.COLUMN_NAME_INTIME + " DESC",
						"1");
    	cursor.moveToFirst();
    	ContentValues values = new ContentValues();
    	
		//Create a new row if both in and out are populated
		//or the db is empty
		if (cursor.getCount() == 0 || !cursor.isNull(1)) {
			values.put(Timestamp.COLUMN_NAME_INTIME, timeInMilliS);
			values.put(Timestamp.COLUMN_NAME_TASK, taskId);
			db.insert(Timestamp.TABLE_NAME, null, values);
		} else {
			//otherwise its a check out, update outmillis
			values.put(Timestamp.COLUMN_NAME_OUTTIME, timeInMilliS);
			db.update(Timestamp.TABLE_NAME, values, "_ID = ?", new String[] {cursor.getString(0)});
		}		
	}
	
	/**
	 * Return a cursor with all configured tasks
	 * @param db A readable sqlitedatabase to get tasks from
	 * @return Cursor with the fields: _ID(int), TaskName(String)
	 */
	public Cursor getTasks(SQLiteDatabase db) {
		String[] queryCols=new String[]{Task._ID, Task.COLUMN_NAME_TASKNAME};
		return db.query(Task.TABLE_NAME,
				queryCols, null, null,
				null, null, null);
	}
}
