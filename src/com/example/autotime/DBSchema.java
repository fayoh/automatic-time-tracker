package com.example.autotime;

import android.provider.BaseColumns;

public class DBSchema {
    // There is nothing to instantiate, so deny it
    public DBSchema() {}

    /* Timestamp table */
    public static abstract class Timestamp implements BaseColumns {
	public static final String TABLE_NAME          = "timestamps";
	public static final String COLUMN_NAME_INTIME  = "inmillis";
	public static final String COLUMN_NAME_OUTTIME = "outmillis";
	public static final String COLUMN_NAME_TASK    = "task";
    }

    /* Task table */
    public static abstract class Task implements BaseColumns {
	public static final String TABLE_NAME           = "tasks";
	public static final String COLUMN_NAME_TASKNAME = "taskname";
    }

    /* Workplaces table */
    public static abstract class Workplace implements BaseColumns {
	public static final String TABLE_NAME                 = "workplaces";
	public static final String COLUMN_NAME_WORKPLACE_NAME = "name";
	public static final String COLUMN_NAME_LATITUDE       = "lat"; 
	public static final String COLUMN_NAME_LONGIITUDE     = "lon";
	public static final String COLUMN_NAME_RADIUS         = "radius";	
    }
    
    /* Timestamp+taskname view */
	public static final String TIMESTAMP_TASK_VIEW_NAME = "timestamptask";
    
    /* SQL Create table statements */
    public static final String SQL_CREATE_TASK_TABLE =
	"CREATE TABLE IF NOT EXISTS " + Task.TABLE_NAME + " (" +
	Task._ID                  + " INTEGER PRIMARY KEY," +
	Task.COLUMN_NAME_TASKNAME + " TEXT)";

    public static final String SQL_CREATE_TIMESTAMP_TABLE =
	"CREATE TABLE IF NOT EXISTS "  + Timestamp.TABLE_NAME + " (" +
	Timestamp._ID                  + " INTEGER PRIMARY KEY," +
	Timestamp.COLUMN_NAME_INTIME   + " INTEGER," +
	Timestamp.COLUMN_NAME_OUTTIME  + " INTEGER," +	
	Timestamp.COLUMN_NAME_TASK     + " INTEGER," +
	"FOREIGN KEY(" + Timestamp.COLUMN_NAME_TASK + ")" +
	"REFERENCES " + Task.TABLE_NAME + "(" + Task._ID + "))";

    public static final String SQL_CREATE_WORKPLACE_TABLE = 
	"CREATE TABLE IF NOT EXISTS " + Workplace.TABLE_NAME + " (" +
	Workplace._ID                        + " INTEGER PRIMARY KEY," +
	Workplace.COLUMN_NAME_WORKPLACE_NAME + " FLOAT,"   +
	Workplace.COLUMN_NAME_LATITUDE       + " FLOAT,"   +
	Workplace.COLUMN_NAME_RADIUS         + " INTEGER" + ")"; 
    
    public static final String SQL_CREATE_TIMESTAMP_VIEW = 
	"CREATE VIEW IF NOT EXISTS timestamptask AS "+
	"SELECT " + Timestamp.TABLE_NAME + "._ID," +
	  Timestamp.COLUMN_NAME_INTIME + "," +
	  Timestamp.COLUMN_NAME_OUTTIME +"," +
	  Task.COLUMN_NAME_TASKNAME +
	" FROM " + Timestamp.TABLE_NAME + " JOIN " + Task.TABLE_NAME +
	" ON  " + Timestamp.COLUMN_NAME_TASK + " = " + Task.TABLE_NAME +
	"." + Task._ID;
   
    public static final String SQL_POPULATE_TASK_TABLE1 =
	"INSERT INTO " + Task.TABLE_NAME + " VALUES (NULL, \"Project 1\")";

    public static final String SQL_POPULATE_TASK_TABLE2 =
	"INSERT INTO " + Task.TABLE_NAME + " VALUES (NULL, \"Project 2\")";
    
    public static final String SQL_POPULATE_TASK_TABLE3 =
	"INSERT INTO " + Task.TABLE_NAME + " VALUES (NULL, \"Project 3\")";
}
