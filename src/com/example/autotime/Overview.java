package com.example.autotime;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.example.autotime.DBSchema.Task;
import com.example.autotime.DBSchema.Timestamp;

public class Overview extends Activity {

    DBHelper mDBHelper;
    CustomCursorAdapter mAdapter;
    Spinner mTaskSpinner;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);
        // TODO Get initialization off main thread
        // TODO So it seems cursors should be managed, fix that
        mDBHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        
        //Bind timestamp listview
        Cursor timestampCursor = mDBHelper.getTimestampsToday(db);
        mAdapter = new CustomCursorAdapter(this, timestampCursor);
        ListView listview = (ListView)findViewById(R.id.timestampstoday);
        listview.setAdapter(mAdapter);
        
        //Bind task spinner
        Cursor taskCursor = mDBHelper.getTasks(db);
        mTaskSpinner = (Spinner)findViewById(R.id.taskSelector);
        @SuppressWarnings("deprecation") //TODO update when we are on API 11
		SimpleCursorAdapter taskAdapter = new SimpleCursorAdapter(
        		this,
        		android.R.layout.simple_spinner_item,
        		taskCursor,
        		new String[] {Task.COLUMN_NAME_TASKNAME},
        		new int[] {android.R.id.text1});
        taskAdapter.setDropDownViewResource(
        		android.R.layout.simple_spinner_dropdown_item);
        mTaskSpinner.setAdapter(taskAdapter);
    }
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.overview, menu);
        return true;
    }
    

    /**
     * Register a timestamp.
     * 
     * This function will add a timestamp to the db,
     * if the user is currently not "in" a new session will be started
     * with currently selected task.
     * 
     * If the user is currently "in" the session will be closed,
     * current value of taskchooser is ignored. If one wishes to 
     * change task the current implementation needs you to "check out"
     * and the "check in" again.
     * 
     * @param view
     */
    @SuppressWarnings("deprecation")
	public void checkinout(View view){
    	//Get current time
    	Calendar cal      = Calendar.getInstance();
    	long timeInMilliS = cal.getTimeInMillis();
    	Log.d("autotime", "get id");
    	//TODO Does this work by chance or is it a good way?
    	int taskId        = (int) mTaskSpinner.getSelectedItemId();
    	Log.d("autotime", "got id: " + taskId);
    	//Update db
    	SQLiteDatabase db = mDBHelper.getWritableDatabase();
    	mDBHelper.addTimestamp(db, timeInMilliS, taskId);
		//Update listview
		//TODO change to loaders when we get on api lvl 11
		mAdapter.getCursor().requery();
    }

    /**
     * Class binding timestamps in db to main listview
     * @author Daniel Bengtsson
     *
     */
	
	//TODO change name to something sensible
    public class CustomCursorAdapter extends CursorAdapter {
    	 
        @SuppressWarnings("deprecation") // We support API level 9
		public CustomCursorAdapter(Context context, Cursor c) {
            super(context, c);
        }
          
		@Override
		@SuppressLint("SimpleDateFormat")
		public void bindView(View view, Context context, Cursor cursor) {
            TextView timeStamp   = (TextView) view;
            Calendar cal         = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

            long dateTimeMillis  = 
            		cursor.getLong(cursor.getColumnIndex(Timestamp.COLUMN_NAME_INTIME));
            cal.setTimeInMillis(dateTimeMillis);
            String inStamp  =  sdf.format(cal.getTime());
            String outStamp = "current";
            
            if(!cursor.isNull(2)) {
            	dateTimeMillis  =
            		cursor.getLong(cursor.getColumnIndex(Timestamp.COLUMN_NAME_OUTTIME));
            	cal.setTimeInMillis(dateTimeMillis);
            	outStamp  =  sdf.format(cal.getTime());
            }
            timeStamp.setText(inStamp + " - " + outStamp + " - " +
            cursor.getString(cursor.getColumnIndex(Task.COLUMN_NAME_TASKNAME)));
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View retView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
     
            return retView;
		}
    }
}
